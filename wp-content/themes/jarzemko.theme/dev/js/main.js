jQuery(document).ready(function($) {
    var GLOBALS = {
        isElementInViewport: function(el) {
            //special bonus for those using jQuery
            if (typeof jQuery === "function" && el instanceof jQuery) {
                el = el[0];
            }
            var rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        },
        $window: $(window),
        $loading: $('.loading'),
        $header: $('.site-header'),
        $menu: $('.menu'),
        $container: $('.container'),
        ScrollController: new ScrollMagic.Controller()
    };
    var APP = {
        InitSite: function() {
            this.Loaded();
            this.Crossbrowser();
            this.Dimensions();
            this.Scrolling();
            this.Navi();
            this.ThreeModels();
            //this.ContactForm();
            this.PageVisibility();
            this.ConsoleMessage();
        },
        Loaded: function() {
            GLOBALS.$window.on('load', function() {
                showSite();
                loadMapAsync();
            });

            function loadMapAsync() {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
                    'callback=CustomMap';
                document.body.appendChild(script);
            }

            function showSite() {
                GLOBALS.$loading.velocity({
                    opacity: 0
                }, {
                    duration: 600,
                    display: 'none',
                    complete: function() {
                        $('.main-container').velocity({
                            opacity: 1
                        }, {
                            duration: 600,
                        });
                    }
                });
            }
        },
        Crossbrowser: function() {
            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = (function() {
                    return window.webkitRequestAnimationFrame ||
                        window.mozRequestAnimationFrame ||
                        window.oRequestAnimationFrame ||
                        window.msRequestAnimationFrame ||
                        function( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
                            window.setTimeout(callback, 1000 / 60);
                        };
                })();
            }
        },
        Dimensions: function() {

            setDimensions();

            GLOBALS.$window.on('resize', function() {
                setDimensions();
            });

            function setDimensions() {
                GLOBALS.$loading.height(GLOBALS.$window.height() + 1);

                if(is.desktop()) {
                    $('.landing').outerHeight(GLOBALS.$window.height());
                }
            }
        },
        Scrolling: function() {
            GLOBALS.$window.on('scroll', function() {
                var scroll = GLOBALS.$window.scrollTop();

                if (scroll > 0) {
                    GLOBALS.$header.addClass('site-header--scrolled');
                } else {
                    GLOBALS.$header.removeClass('site-header--scrolled');
                }
            });
            GLOBALS.$window.on('load', function() {
                if(is.desktop()) {
                    new ScrollMagic.Scene({
                        duration: $('.about__wrap').outerHeight() - $('.scrollbar__container').outerHeight(),
                        offset: $('.about').offset().top
                    })
                    .setPin(".scrollbar__container")
                    .addTo(GLOBALS.ScrollController);
                }
            })

        },
        ThreeModels: function() {
            var $wrap = $('.employees__model');

            $wrap.each(function() {
                var $self = $(this),
                    loader = new THREE.JSONLoader(true),
                    width = $self.width();

                $self.height(width);

                if(Modernizr.webgl) {
                    loader.loadAjaxJSON(loader, $self.data('model-url'), function(geometry) {
                        var scene = new THREE.Scene(),
                            camera = new THREE.PerspectiveCamera(45, width / width, 0.1, 1000),
                            renderer = new THREE.WebGLRenderer({
                                antialias: true
                            }),
                            meshRotation = 45;

                        renderer.setSize(width, width);
                        renderer.setClearColor(0x182a22, 1);
                        $self.append(renderer.domElement);

                        camera.position.z = 5;
                        lights(scene);

                        var material = new THREE.MeshLambertMaterial({
                            color: 0x474747
                        });

                        var mesh = new THREE.Mesh(geometry, material);

                        mesh.rotation.y = meshRotation;
                        mesh.scale.set(1.6, 1.6, 1.6);
                        mesh.position.set(0, -1.8, 0);
                        scene.add(mesh);

                        var ISeeYou = new TWEEN.Tween(mesh.rotation)
                            .to({
                                y: meshRotation - 1
                            }, 400)
                            .easing(TWEEN.Easing.Quadratic.In);

                        GLOBALS.$window.on('scroll', function() {
                            if (GLOBALS.isElementInViewport($self)) {

                                $self.addClass('employees__model--visible');

                                var visibleIndex = $self.index('.employees__model--visible');
                                    ISeeYou.to({
                                        y: meshRotation - 1
                                    }).delay(visibleIndex * 200).start();
                            } else {
                                $self.removeClass('employees__model--visible');
                                    ISeeYou.to({
                                        y: meshRotation
                                    }).start();
                            }
                        });

                        function render() {
                            requestAnimationFrame(render);
                            TWEEN.update();
                            renderer.render(scene, camera);
                        }

                        render();
                    });
                } else {
                    var bgUrl = $self.data('bg');

                    $self.css({
                        backgroundImage: bgUrl
                    })
                }
            });

            function lights(scene) {
                var keyLight = new THREE.SpotLight(0xffffff, 3.5);

                keyLight.position.set(-100, 100, 100);
                scene.add(keyLight);

                var fillLight = new THREE.SpotLight(0xffffff, 2);

                fillLight.position.set(100, 0, 100);
                scene.add(fillLight);

                var rimLight = new THREE.SpotLight(0xffffff, 2);

                rimLight.position.set(0, 100, -100);
                scene.add(rimLight);
            }
        }, //ThreeModels
        Navi: function() {
            var $menuBtn = $('.menu__btn'),
                $insideLink = $('a[href*=#]:not([href=#])');

            $menuBtn.on('click', function() {
                GLOBALS.$menu.toggleClass('menu--expanded');
                GLOBALS.$header.toggleClass('site-header--expanded');
                $(this).toggleClass('menu__btn--close');
            });

            $insideLink.on('click', function(event) {
                var target = $(this).attr('href'),
                    speed = 3,
                    distance = Math.abs($(target).offset().top - GLOBALS.$window.scrollTop()),
                    duration = distance / speed;

                $(target).velocity("scroll", {
                    duration: duration,
                    easing: "ease-in",
                    offset: -(GLOBALS.$header.height() * 3)
                });

                event.preventDefault();
            });

            // new ScrollMagic.Scene({
            //     duration: $('.about__wrap').outerHeight() - $('.scrollbar__container').outerHeight(),
            //     offset: $('.about').offset().top
            // })
            // .setPin(".scrollbar__container")
            // .addTo(GLOBALS.ScrollController);

        },
        // ContactForm: function() {
        //     _initContactForm();
        //
        //     function _initContactForm() {
        //
        //         _sliders();
        //         _steps();
        //
        //         function _sliders() {
        //             var $deadline = $('.contact__slider--deadline'),
        //                 $budget = $('.contact__slider--budget');
        //
        //             $deadline.noUiSlider({
        //                 start: [4, 6],
        //                 step: 1,
        //                 range: {
        //                     'min': [3],
        //                     'max': [12]
        //                 }
        //             });
        //
        //             $budget.noUiSlider({
        //                 start: [4000, 7000],
        //                 step: 1000,
        //                 range: {
        //                     'min': [3000],
        //                     'max': [35000]
        //                 }
        //             });
        //
        //             $(".contact__slider").each(function() {
        //                 var $this = $(this),
        //                     $valContainer = $this.siblings('.contact__val'),
        //                     $lowerInput = $valContainer.children('.contact__input--lower'),
        //                     $upperInput = $valContainer.children('.contact__input--upper');
        //
        //                 $this.Link('lower').to($lowerInput, _handleSliderValueLower).Link('upper').to($upperInput, _handleSliderValueUpper);
        //
        //                 function _handleSliderValueLower(value) {
        //                     $lowerInput.text(parseInt(value) + ' ' + $lowerInput.data('unit') + ' - ');
        //                 }
        //
        //                 function _handleSliderValueUpper(value) {
        //                     $upperInput.text(parseInt(value) + ' ' + $upperInput.data('unit'));
        //                 }
        //             });
        //         }
        //
        //         function _steps() {
        //             var current_fs, next_fs, prev_fs, animating, direction;
        //
        //             $('.contact__step').on('click', function(event) {
        //                 event.preventDefault();
        //
        //                 if (animating) return false;
        //                 animating = true;
        //
        //
        //                 current_fs = $(this).parent();
        //                 next_fs = $(this).parent().next();
        //                 prev_fs = $(this).parent().prev();
        //                 direction = $(this).data('target');
        //
        //                 _changeStep(direction);
        //
        //             });
        //
        //             function _changeStep() {
        //                 current_fs.velocity({
        //                     opacity: 0
        //                 }, {
        //                     complete: function() {
        //                         var target;
        //                         if (direction === 'next') {
        //                             target = next_fs;
        //                         } else if (direction === 'prev') {
        //                             target = prev_fs;
        //                         }
        //
        //                         target.velocity({
        //                             opacity: 1
        //                         }, {
        //                             display: 'table'
        //                         });
        //                         _updateStepIndicator(target);
        //                         animating = false;
        //                     }
        //                 }, {
        //                     display: "none"
        //                 });
        //             }
        //
        //             function _updateStepIndicator(el) {
        //                 var $progress = $('.contact__progress');
        //                 $progress.text(el.index() + '/4');
        //             }
        //         }
        //     }
        // },
        PageVisibility: function() {
            document.addEventListener("visibilitychange", function() {
                if (document.hidden) {

                } else {

                }
            });
        },
        ConsoleMessage: function() {
            console.log("");
        }
    };

    APP.InitSite();
});

function CustomMap() {
    var lat = jQuery('#map-canvas').data('lat'),
        lng = jQuery('#map-canvas').data('lng'),
        marker_img = jQuery('#map-canvas').data('marker');

    var myLatlng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        zoom: 17,
        draggable: false,
        scrollwheel: false,
        center: new google.maps.LatLng(lat, lng),
        disableDefaultUI: true,
        styles: [{
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#182a22"
            }]
        }, {
            "featureType": "poi",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{
                "color": "#325746"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#508C72"
            }]
        }, {
            "featureType": "poi",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "administrative",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#182a22"
            }]
        }, {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "transit",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "visibility": "off"
            }]
        }]
    };


    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Jarzemko',
        icon: marker_img
    });

    google.maps.event.trigger(map, "resize");
}
