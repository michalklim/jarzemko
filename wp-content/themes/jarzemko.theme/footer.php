<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package themeHandle
 */
?>
	</div>
</div>
<?php get_template_part( 'icons'); ?>
<?php wp_footer(); ?>
</body>
</html>
