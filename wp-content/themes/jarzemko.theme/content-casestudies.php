<?php
/**
 * Case Studies Section
 *
 * @package Jarzemko
 */
?>
<section id="work" class="work container">
    <?php $work_query = new WP_Query( 'pagename=work' );

    // The Loop
    if ( $work_query->have_posts() ) {
        while ( $work_query->have_posts() ) {
            $work_query->the_post(); ?>
            <div class="section__title">
                <?php echo get_post_meta( get_the_ID(), 'app_page_title', true ); ?>
            </div>
            <div class="work__wrap">
                <article class="col col-6 work__item work__item--title">
                    <?php the_content(); ?>
                </article>
        <?php
        }
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    // The Query
    $the_query = new WP_Query( 'post_type=case-studies' );

    // The Loop
    if ( $the_query->have_posts() ) {
    	while ( $the_query->have_posts() ) {
    		$the_query->the_post();
            $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');  ?>
            <article class="col col-4 work__item" style="background-image:url(<?php echo $url[0] ?>);">
                <a href="<?php the_permalink() ?>">
                    <div class="work__coverer"></div>
                    <div class="work__info">
                        <div class="work__row">
                            <span class="work__val"><h4><?php the_title() ?></h4></span>
                        </div>
                        <div class="work__row">
                            <span class="work__label">Co?</span>
                            <span class="work__val"><?php echo get_post_meta( get_the_ID(), 'app_case-studies_what', true ); ?></span>
                        </div>
                        <div class="work__row">
                            <span class="work__label">Klient</span>
                            <span class="work__val"><?php echo get_post_meta( get_the_ID(), 'app_case-studies_client', true ); ?></span>
                        </div>
                        <div class="work__row">
                            <span class="work__label">Rok</span>
                            <span class="work__val"><?php echo get_post_meta( get_the_ID(), 'app_case-studies_year', true ); ?></span>
                        </div>
                    </div>
                </a>
            </article>
            <?php
    	}
    	echo '</div>';
    }
    /* Restore original Post Data */
    wp_reset_postdata();
    ?>
</section>
