<?php
/**
 * The header template
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package themeHandle
 */
?>
<!DOCTYPE html>

<!--[if lt IE 9]>
<html id="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title('|', true, 'right'); ?></title>
    <script>
      WebFontConfig = {
        typekit: { id: 'tpu4aky' }
      };

      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>
    <!-- favicon & links -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!-- stylesheets are enqueued via functions.php -->

    <!-- all other scripts are enqueued via functions.php -->
    <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/html5shiv.js" type="text/javascript"></script>
    <![endif]-->

    <?php // Lets other plugins and files tie into our theme's <head>:
wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="loading">
    <div class="loading__indicator">
      <img src="wp-content/themes/jarzemko.theme/assets/images/loading.gif" />
    </div>
  </div>
  <div class="container main-container">
    <header class="site-header">
      <div class="site-header__logo">
        <a href="#landing">
          <svg class="main__symbol">
            <use xlink:href="#jarzemko_symbol" />
          </svg>
        </a>
        <svg class="main__logo vanishAfterScroll">
          <use xlink:href="#jarzemko_logo" />
        </svg>
      </div>
      <div class="site-header__menu-btn">
        <div class="menu__btn">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <span class="vanishAfterScroll">Menu</span>
      </div>
    </header>
    <?php

    $defaults = array(
    	'theme_location'  => 'primary',
    	'menu'            => '',
    	'container'       => 'nav',
    	'container_class' => 'menu',
    	'container_id'    => '',
    	'menu_class'      => 'menu__wrap',
    	'menu_id'         => '',
    	'echo'            => true,
    	'fallback_cb'     => 'wp_page_menu',
    	'before'          => '',
    	'after'           => '',
    	'link_before'     => '',
    	'link_after'      => '',
    	'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
    	'depth'           => 0,
    	'walker'          => ''
    );

    wp_nav_menu( $defaults );
    ?>
    <div class="wrapper">
