<?php
/**
 * Welcome Section
 *
 * @package Jarzemko
 */
?>

<section id="landing" class="landing container">
	<div class="landing__wrap">
		<?php
		// The Query
		$landing_query = new WP_Query( 'pagename=landing' );

		// The Loop
		if ( $landing_query->have_posts() ) {
			while ( $landing_query->have_posts() ) {
				$landing_query->the_post();
				?>
				<video autoplay loop>
					<source src="<?php echo get_post_meta( get_the_ID(), 'app_video_mp4', true ); ?>" type="video/mp4">
					<source src="<?php echo get_post_meta( get_the_ID(), 'app_video_webm', true ); ?>" type="video/webm">
					<source src="<?php echo get_post_meta( get_the_ID(), 'app_video_ogg', true ); ?>" type="video/ogg">
				</video>
				<div class="landing__splash-text">
					<?php the_content(); ?>
				</div>
				<?php
			}
		}
		/* Restore original Post Data */
		wp_reset_postdata();
		?>
	</div>
</section>
