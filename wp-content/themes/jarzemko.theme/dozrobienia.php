<div class="col col-6 contact__section">
	<form class="form contact__form">
		<span class="contact__progress">
			1/4
		</span>
		<!--What do you need? fieldset-->
		<fieldset class="contact__fieldset">
			<h3 class="contact__title">Czego potrzebujesz?</h3>
			<textarea>

			</textarea>
			<a href="#" data-target="next" class="button contact__step">
				<div class="button__first-side">Dalej</div>
				<div class="button__second-side">Deadline</div>
			</a>
		</fieldset>
		<!--How fast? fieldset-->
		<fieldset class="contact__fieldset">
			<h3 class="contact__title">Jak szybko?</h3>
			<h6 class="contact__subtitle">Deadline</h6>
			<div class="contact__slider contact__slider--deadline"></div>
			<div class="contact__val">
				<span data-unit="tygodni" class="contact__input--lower"></span>
				<span data-unit="tygodni" class="contact__input--upper"></span>
			</div>

			<a href="#" data-target="next" class="button contact__step">
				<div class="button__first-side">Dalej</div>
				<div class="button__second-side">Deadline</div>
			</a>
		</fieldset>

<fieldset class="contact__fieldset">
	<h3 class="contact__title">Na bogato?</h3>
	<h6 class="contact__subtitle">Budżet</h6>
	<div class="contact__slider contact__slider--budget"></div>
	<div class="contact__val">
		<span data-unit="PLN" class="contact__input--lower"></span>
		<span data-unit="PLN" class="contact__input--upper"></span>
	</div>
	<a href="#" data-target="next" class="button contact__step">
		<div class="button__first-side">Dalej</div>
		<div class="button__second-side">Deadline</div>
	</a>

</fieldset>

<fieldset class="contact__fieldset">
	<h3 class="contact__title">Twoje dane</h3>
	<div class="form__row">
		<label for="name">Imię i Nazwisko</label>
		<input type="text" name="name" placeholder="np. Jan Kowalski" autocomplete="name"/>
	</div>
	<div class="form__row">
		<label for="name">Adres e-mail</label>
		<input type="email" name="email" placeholder="np. jan.kowalski@mail.com" />
	</div>
	<div class="form__row">
		<label for="name">Telefon</label>
		<input type="tel" pattern='[0-9]+' name="phone" placeholder="np. 00 48 123 456 768"/>
	</div>
	<a href="#" class="button contact__submit" role="button">
		<div class="button__first-side">Wyślij</div>
		<div class="button__second-side">Zatrudnij nas</div>
	</a>
</fieldset>

<fieldset class="contact__fieldset">
	<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
		Corporis harum error non asperiores voluptatem illum
		doloribus sed quis inventore totam optio laboriosam
		facere necessitatibus incidunt, eveniet, sequi ducimus
		ipsam quasi.</h5>

	</fieldset>
</form>
</div>
