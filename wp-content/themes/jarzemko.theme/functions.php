<?php
/**
 * Theme functions
 *
 * Sets up the theme and provides some helper functions.
 *
 * @package themeHandle
 */
/* THEME SETUP
 ========================== */
if (!function_exists('themeFunction_setup')):
    function themeFunction_setup() {
        // Available for translation
        load_theme_textdomain('themeTextDomain', get_template_directory() . '/languages');
        // Add default posts and comments RSS feed links to <head>.
        add_theme_support('automatic-feed-links');
        // Add custom nav menu support
        register_nav_menu('primary', __('Primary Menu', 'themeTextDomain'));
        // Add featured image support
        add_theme_support('post-thumbnails');
        // Add custom image sizes
        // add_image_size( 'name', 500, 300 );

    }
endif;
add_action('after_setup_theme', 'themeFunction_setup');
/* ENQUEUE SCRIPTS & STYLES
 ========================== */
function themeFunction_scripts() {
    // theme style.css file
    wp_enqueue_style('themeTextDomain-style', get_stylesheet_uri());
    // threaded comments
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
    // vendor scripts
    wp_enqueue_script('vendor', get_template_directory_uri() . '/assets/vendor/plugins.js', array('jquery'));
    // theme scripts
    wp_enqueue_script('theme-init', get_template_directory_uri() . '/assets/app.js', array('vendor'));
}
add_action('wp_enqueue_scripts', 'themeFunction_scripts');

/* MISC EXTRAS
 ========================== */
// Optional Customizations
// Includes: TinyMCE tweaks, admin menu & bar settings, query overrides
include ('inc/functions/customizations.php');


/* HELPER FUNCTIONS
========================== */

function get_ID_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}
/* CUSTOM METABOXES
========================== */
if ( file_exists(  __DIR__ .'/inc/lib/cmb2/init.php' ) ) {
    require_once  __DIR__ .'/inc/lib/cmb2/init.php';
} elseif ( file_exists(  __DIR__ .'/inc/lib/CMB2/init.php' ) ) {
    require_once  __DIR__ .'/inc/lib/CMB2/init.php';
}

function APP_metaboxes( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = 'app';

    /**
    * Sample metabox to demonstrate each field type included
    */
    $meta_boxes['video_on_landing'] = array(
        'id'            => 'video_metabox',
        'title'         => __( 'Video', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'id', 'value' => get_ID_by_slug('landing')),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'fields'        => array(
            array(
                'name'    => 'Mp4',
                'id'      => $prefix . '_video_mp4',
                'type'    => 'file',
            ),
            array(
                'name'    => 'Ogg',
                'id'      => $prefix . '_video_ogg',
                'type'    => 'file',
            ),
            array(
                'name'    => 'WebM',
                'id'      => $prefix . '_video_webm',
                'type'    => 'file',
            )
        )
    );
    $meta_boxes['people_metaboxes'] = array(
        'id'            => 'people_metaboxes',
        'title'         => __( 'Dodatkowe informacje', 'cmb2' ),
        'object_types'  => array( 'people' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'fields'        => array(
            array(
                'name'    => 'Model 3D',
                'id'      => $prefix . '_people_3dmodel',
                'type'    => 'file',
            ),
            array(
                'name'    => 'Funkcja',
                'id'      => $prefix . '_people_job',
                'type'    => 'text',
            )
        )
    );
    $meta_boxes['casetudies_metaboxes'] = array(
        'id'            => 'casestudies_metaboxes',
        'title'         => __( 'Dodatkowe informacje', 'cmb2' ),
        'object_types'  => array( 'case-studies' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'fields'        => array(
            array(
                'name'    => 'Co?',
                'id'      => $prefix . '_case-studies_what',
                'type'    => 'text',
            ),
            array(
                'name'    => 'Klient',
                'id'      => $prefix . '_case-studies_client',
                'type'    => 'text',
            ),
            array(
                'name'    => 'Rok',
                'id'      => $prefix . '_case-studies_year',
                'type'    => 'text',
            )
        )
    );
    $meta_boxes['page_metaboxes'] = array(
        'id'            => 'page_metaboxes',
        'title'         => __( 'Ogolne informacje', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'fields'        => array(
            array(
                'name'    => 'Dodatkowy tytuł',
                'id'      => $prefix . '_page_title',
                'type'    => 'text',
            )
        )
    );
    $meta_boxes['contact_page_metabox'] = array(
        'id'           => 'map_page_metabox',
        'title'        => __( 'Mapa', 'cmb2' ),
        'object_types' => array( 'page', ), // Post type
        'context'      => 'normal',
        'priority'     => 'high',
        'show_names'   => true, // Show field names on the left
        'show_on'      => array( 'id' => array( 25 ) ), // Specific post IDs to display this metabox
        'fields'       => array(
            array(
                'name' => 'Położenie',
                'desc' => 'Przesun marker aby ustawic dokladna lokalizacje',
                'id' => $prefix . '_location',
                'type' => 'pw_map'
            ),
            array(
                'name' => 'Obraz markera',
                'id' => $prefix . '_marker_image',
                'type' => 'file'
            ),
        )
    );


    return $meta_boxes;

}

add_filter( 'cmb2_meta_boxes', 'APP_metaboxes' );


/* CUSTOM TAXONOMIES & POSTS
 ========================== */
function create_custom_post() {
    $PeopleLabels = array(
        'name'               => _x( 'Osoby', 'post type general name', 'app' ),
        'singular_name'      => _x( 'Osobę', 'post type singular name', 'app' ),
        'menu_name'          => _x( 'Osoby', 'admin menu', 'app' ),
        'name_admin_bar'     => _x( 'Osoby', 'add new on admin bar', 'app' ),
        'add_new'            => _x( 'Dodaj nową osobę', 'book', 'app' ),
        'add_new_item'       => __( 'Dodaj nową osobę', 'app' ),
        'new_item'           => __( 'Nowa osoba', 'app' ),
        'edit_item'          => __( 'Edytuj osobę', 'app' ),
        'view_item'          => __( 'Pokaż osobę', 'app' ),
        'all_items'          => __( 'Wszystkie osoby', 'app' ),
        'search_items'       => __( 'Przeszukaj osoby', 'app' ),
        'parent_item_colon'  => __( 'Parent', 'app' ),
        'not_found'          => __( 'Nie znaleziono osób', 'app' ),
        'not_found_in_trash' => __( 'Nie znaleziono osób w koszu.', 'app' )
    );

    $PeopleArgs = array(
        'labels'             => $PeopleLabels,
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => false,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'thumbnail')
    );

    register_post_type( 'people', $PeopleArgs );


    $CaseStudiesLabels = array(
        'name'               => _x( 'Case Studies', 'post type general name', 'app' ),
        'singular_name'      => _x( 'Case Study', 'post type singular name', 'app' ),
        'menu_name'          => _x( 'Case Studies', 'admin menu', 'app' ),
        'name_admin_bar'     => _x( 'Case Studies', 'add new on admin bar', 'app' ),
        'add_new'            => _x( 'Dodaj nowe case study', 'case-studies', 'app' ),
        'add_new_item'       => __( 'Dodaj nowe case study', 'app' ),
        'new_item'           => __( 'Nowe case study', 'app' ),
        'edit_item'          => __( 'Edytuj case study', 'app' ),
        'view_item'          => __( 'Pokaż case study', 'app' ),
        'all_items'          => __( 'Wszystkie case studies', 'app' ),
        'search_items'       => __( 'Przeszukaj case studies', 'app' ),
        'parent_item_colon'  => __( 'Parent', 'app' ),
        'not_found'          => __( 'Nie znaleziono case study', 'app' ),
        'not_found_in_trash' => __( 'Nie znaleziono case study w koszu.', 'app' )
    );

    $CaseStudiesArgs = array(
        'labels'             => $CaseStudiesLabels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => false,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'case-studies', $CaseStudiesArgs );
}

add_action( 'init', 'create_custom_post' );

/* THEME OPTIONS PAGE
 using CMB2
========================== */

class app_Admin {
    /**
     * Option key, and option page slug
     * @var string
     */
    private $key = 'app_options';
    /**
     * Options page metabox id
     * @var string
     */
    private $metabox_id = 'app_option_metabox';
    /**
     * Options Page title
     * @var string
     */
    protected $title = '';
    /**
     * Options Page hook
     * @var string
     */
    protected $options_page = '';
    /**
     * Constructor
     * @since 0.1.0
     */
    public function __construct() {
    	// Set our title
    	$this->title = __( 'Opcje Strony', 'app' );
    }
    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
    	add_action( 'admin_init', array( $this, 'init' ) );
    	add_action( 'admin_menu', array( $this, 'add_options_page' ) );
    	add_action( 'cmb2_init', array( $this, 'add_options_page_metabox' ) );
    }
    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {
    	register_setting( $this->key, $this->key );
    }
    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
    	$this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
    }
    /**
     * Admin page markup. Mostly handled by CMB2
     * @since  0.1.0
     */
    public function admin_page_display() {
    	echo '<div class="wrap cmb2_options_page' . $this->key . '">';
         echo '<h2>' . esc_html( get_admin_page_title() ) . '</h2>';
    	    echo cmb2_metabox_form( $this->metabox_id, $this->key );
    	echo '</div>';
    }
    /**
     * Add the options metabox to the array of metaboxes
     * @since  0.1.0
     */
    function add_options_page_metabox() {
    	$cmb = new_cmb2_box( array(
    		'id'      => $this->metabox_id,
    		'hookup'  => false,
    		'show_on' => array(
    			// These are important, don't remove
    			'key'   => 'options-page',
    			'value' => array( $this->key, )
    		),
    	) );
    	// Set our CMB2 fields
        $cmb->add_field( array(
         'name' => 'Loading',
         'desc' => 'Dodaj loader',
         'id' => 'app_loader',
         'type' => 'file',
         'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
        ) );
    }
    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {
    	// Allowed fields to retrieve
    	if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
    		return $this->{$field};
    	}
    	throw new Exception( 'Invalid property: ' . $field );
    }
 }
 // Get it started
 $GLOBALS['app_Admin'] = new app_Admin();
 $GLOBALS['app_Admin']->hooks();
 /**
  * Helper function to get/return the app_Admin object
  * @since  0.1.0
  * @return app_Admin object
  */
 function app_Admin() {
 	global $app_Admin;
 	return $app_Admin;
 }
 /**
  * Wrapper function around cmb2_get_option
  * @since  0.1.0
  * @param  string  $key Options array key
  * @return mixed        Option value
  */
 function app_get_option( $key = '' ) {
 	global $app_Admin;
 	return cmb2_get_option( $app_Admin->key, $key );
 }
