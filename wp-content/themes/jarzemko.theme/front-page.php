<?php
/**
 * Home Page
 *
 * @package themeHandle
 */
get_header();

get_template_part( 'content', 'landing' );
get_template_part( 'content', 'about' );
get_template_part( 'content', 'casestudies' );
get_template_part( 'content', 'contact' );
?>

<?php get_footer(); ?>
