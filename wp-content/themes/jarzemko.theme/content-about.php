<?php
/**
 * About Section
 *
 * @package Jarzemko
 */
?>

<section id="about" class="about container">
    <?php
    // The Query
    $about_query = new WP_Query( 'pagename=about' );

    // The Loop
    if ( $about_query->have_posts() ) {
        while ( $about_query->have_posts() ) {
            $about_query->the_post(); ?>
            <div class="section__title">
                <?php echo get_post_meta( get_the_ID(), 'app_page_title', true ); ?>
            </div>

                <div class="col-4 about__desc scrollbar">
                    <div class="scrollbar__container">
                        <?php the_content();
            } ?>
                    </div>
                </div>
    <div class="about__wrap col col-8">
    <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    // The Query
    $people_query = new WP_Query( array(
        'order'   => 'ASC',
        'post_type'  => 'people',
    ));

    // The Loop
    if ( $people_query->have_posts() ) {
            while ( $people_query->have_posts() ) {
                $people_query->the_post();
            ?>
            <div class="gallery-6 employees__employee">
                <div data-model-url="<?php echo get_post_meta( get_the_ID(), 'app_people_3dmodel', true ); ?>" class="employees__model">
                </div>
                <div class="employees__desc">
                    <h5><?php the_title(); ?></h5>
                    <small><?php echo get_post_meta( get_the_ID(), 'app_people_job', true ); ?></small>
                </div>
            </div>
            <?php
            }

    }
    /* Restore original Post Data */
    wp_reset_postdata();
        ?>
    </div>
</section>
