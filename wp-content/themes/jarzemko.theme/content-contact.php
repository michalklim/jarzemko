<?php
/**
 * Contact Section
 *
 * @package Jarzemko
 */
?>

<section id="contact" class="contact container">
	<?php
	// The Query
	$contact_query = new WP_Query( 'pagename=contact' );

	// The Loop
	if ( $contact_query->have_posts() ) {
		while ( $contact_query->have_posts() ) {
			$contact_query->the_post();
			?>
			<div class="section__title">
				<?php echo get_post_meta( get_the_ID(), 'app_page_title', true ); ?>
		    </div>
			<div class="contact__wrap">
				<?php $location = get_post_meta( get_the_ID(), 'app_location', true ); ?>
				<div id="map-canvas" class="col col-8 contact__section contact__map" data-lat="<?php echo $location['latitude']; ?>" data-lng="<?php echo $location['longitude']; ?>" data-marker="<?php echo get_post_meta( get_the_ID(), 'app_marker_image', true ); ?>">

				</div>
				<div class="col-4 contact__section contact__desc">
					<?php the_content(); ?>
				</div>
			</div>
			<?php
		}
	}
	/* Restore original Post Data */
	wp_reset_postdata();
	?>
</section>
